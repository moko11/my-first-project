global class csStorageReportEmailReceive implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.Inboundenvelope envelope) {
        
        
        String strAccount = '';
        
        Account account = new Account();
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        String errMessage = '';
        //String plainText_converter = email.plainTextBody;
        result.success = false;
        
        try {
            if(email.plainTextBody.contains('&')){
                email.plainTextBody = email.plainTextBody.replaceall('&', 'amp;');
            }
            reader = new XmlStreamReader(email.plainTextBody);
            
            
            m_csSANReport report = parseReport();
            strAccount = report.name;
            
            set<Folder__c> accountFolders = new set<Folder__c>([select Id, fPath__c, fVolume__c, fTopLevelFolder__c from Folder__c where fAccount__r.Name = :strAccount]);
            
            String strDate = '';
            
            if (report.name != ''){
                if ([select count() from Account where Name = :strAccount] > 0){
                    account = [select Id from Account where Name = :strAccount][0];
                    
                    if ([select count() from Account where Name = :strAccount] == 1) {
                        List<String> arrRepDate = String.ValueOf(report.repDate).split(' ');// cheking
                        List<Folder__c> fFoldersToInsert = new List<Folder__c>();
                        List<Folder__c> fFoldersToUpdate = new List<Folder__c>();
                        
                        Folder__c fCheckFolder = new Folder__c();
                        
                        for (m_csFolder rFolder : report.folders) {
                            //String strFolderName = rFolder.name;
                            Folder__c srFolder = new Folder__c();
                            srFolder.fAccount__c = account.Id;
                            srFolder.fReportDate__c = Date.valueOf(arrRepDate[0]);
                            
                            //whereAmI += '1_';
                            srFolder.fSize__c = rFolder.space;
                            //whereAmI += '2_';
                            srFolder.fSizeUOM__c = rFolder.spaceUOM;
                            //whereAmI += '3_';
                            //srFolder.fStorageReport__c = StorageReport.Id;
                            //whereAmI += '4_';
                            srFolder.fTopLevelFolder__c = String.valueOf(rFolder.isTopLevelFolder);
                            //whereAmI += '5_';
                            srFolder.fUsedPercentage__c = rFolder.usedPercentage;
                            //whereAmI += '6_';
                            srFolder.fFreeSize__c = rFolder.freeSize;
                            //whereAmI += '7_';
                            srFolder.fFreeSizeUOM__c = rFolder.freeSizeUOM;
                            //whereAmI += '8_';
                            if(rFolder.path.contains('amp;')){
                                rFolder.path=rFolder.path.replace('amp;', '&');
                            }
                            srFolder.fPath__c = rFolder.path;
                            
                            //whereAmI += '9_';$
                            if(rFolder.name.contains('amp;')){
                                rFolder.name=rFolder.name.replace('amp;', '&');
                            }
                            srFolder.Name = rFolder.name;
                            //whereAmI += '10_';
                            srFolder.fVolume__c = String.valueOf(rFolder.isVolume);
                            //whereAmI += '11_';
                            srFolder.fUsedSizeUOM__c = rFolder.usedSizeUOM;
                            //whereAmI += '12_';
                            srFolder.fUsed__c = rFolder.used;
                            //whereAmI += '13_';
                            srFolder.fUsedMB__c = rFolder.usedMB;
                            srFolder.fFreeMB__c = rFolder.freeMB;
                            srFolder.fSizeMB__c = rFolder.spaceMB;
                            
                            
                            for (Folder__c f : accountFolders){
                                if (f.fPath__c == rFolder.path) {
                                    //errMessage += srFolder.Name + ' - ' + f.Id  + ' - ' + f.fVolume__c  + ' - ' + srFolder.fVolume__c + ' | ';
                                    if(String.valueOf(f.fVolume__c) == String.valueOf(srFolder.fVolume__c)){
                                        if(String.valueOf(f.fTopLevelFolder__c) == String.valueOf(srFolder.fTopLevelFolder__c)){
                                            //errMessage += '****' + srFolder.Name + ' - ' + f.Id + ' | ';
                                            srFolder.Id = f.Id;
                                        }
                                    }
                                }
                            }
                            
                            //whereAmI += srFolder.Id + '\n';
                            
                            
                            if (srFolder.Id != null){
                                //update srFolder;
                                fFoldersToUpdate.add(srFolder);
                            }else{
                                //insert srFolder;
                                fFoldersToInsert.add(srFolder);
                            }
                            
                        }
                        
                        if (fFoldersToInsert.size() > 0){
                            insert fFoldersToInsert;
                        }
                        if (fFoldersToUpdate.size() > 0){
                            update fFoldersToUpdate;
                        }
                        
                        //}
                    }
                    else
                    {
                        errMessage = 'More than one account was found in SalesForce with "' + report.name + '" Name. Please check Account Names';
                    }
                }else{
                    errMessage = 'Account was not found in SalesForce : ' + report.name;
                }
                
                if (errMessage == ''){
                    if (whereAmI == ''){
                        result.message = 'No errors on importing ' + report.name + ' report';
                    }else{
                        result.message = 'Error during import ' + report.name + ' report - ' + whereAmI;
                    }
                }else{
                    result.message = 'Error when importing to SalesForce : ' + errMessage;
                }
            }
            
        } catch (Exception e) {
            //result.success = false;
            result.message = 'Error caught when importing to SalesForce : ' + e.getMessage() + ' : ' + whereAmI;
        }
        
        
        result.message += '\n********';
        
        
        updateFoldersWithSizeOfVolumeMB(strAccount);
        
        return result;
    }
    
    public void updateFoldersWithSizeOfVolumeMB(String strAccount){
        
        set<Folder__c> volumeFolders = new set<Folder__c>([select fPath__c, fSizeMB__c from Folder__c where fAccount__r.Name = :strAccount and fVolume__c = 'true']);
        
        if (volumeFolders.size() > 0){
            for (Folder__c f : volumeFolders){
                String volumePath = f.fPath__c;
                
                set<Folder__c> foldersOnVolume = new set<Folder__c>([select fPath__c, fSizeMB__c from Folder__c where fAccount__r.Name = :strAccount and fVolume__c = 'false' and fPath__c like :volumePath + '%']);
                
                if (foldersOnVolume.size() > 0){
                    
                    List<Folder__c> sfFoldersToUpdate = new List<Folder__c>();
                    
                    for (Folder__c sizef : foldersOnVolume){
                        sizef.fRootVolumeSizeMB__c = f.fSizeMB__c;
                        
                        sfFoldersToUpdate.add(sizef);
                    }
                    
                    update sfFoldersToUpdate;
                }  
            }
        }
        
    }
    public class m_csSANReport{
        String name;
        Date repDate;
        m_csFolder[] folders;
    }
    
    public class m_csFolder{
        String id = '';
        String name = '';
        String path = '';
        Decimal freeSize = 0;
        String freeSizeUOM;
        Decimal space = 0;
        String spaceUOM;
        Decimal used = 0;
        String usedSizeUOM;
        Boolean isVolume;
        Boolean isTopLevelFolder;
        Decimal usedPercentage = 0;
        Decimal spaceMB = 0;
        Decimal freeMB = 0;
        Decimal usedMB = 0;
    }
    
    
    XmlStreamReader reader;
    String whereAmI = '';
    
    public m_csSANReport parseReport() {
        m_csSANReport report = new m_csSANReport();
        m_csFolder[] folders = new m_csFolder[0];
        
        boolean isSafeToGetNextXmlElement = true;
        boolean foundSanReportNode = false;
        
        try{
            while(isSafeToGetNextXmlElement) {
                
                if (reader.getEventType() == XmlTag.START_ELEMENT) {
                    if (reader.getLocalName() == 'sanreport' || foundSanReportNode){
                        foundSanReportNode = true;
                        
                        if (reader.getLocalName() == 'reportname') {
                            if (reader.hasNext()) {
                                reader.next();
                            } else {
                                isSafeToGetNextXmlElement = false;
                                break;
                            }
                            report.name = reader.getText();
                        }
                        
                        if (reader.getLocalName() == 'date') {
                            if (reader.hasNext()) {
                                reader.next();
                            } else {
                                isSafeToGetNextXmlElement = false;
                                break;
                            }
                            report.repDate = Date.parse(reader.getText());
                        }
                        
                        
                        if (reader.getLocalName() == 'folders') {
                            //stay in the loop
                            if (reader.hasNext()) {
                                reader.next();
                            }
                            
                            while (reader.getLocalName() == 'folder'){
                                m_csFolder folder1 = parseFolderVolume(false, report.name);
                                folders.add(folder1);
                                reader.next();
                            }
                        }
                        
                        /**                  
if (reader.getLocalName() == 'folders2') {
//stay in the loop
if (reader.hasNext()) {
reader.next();
}


while (reader.getLocalName() == 'folder'){
m_csFolder folder2 = parseFolderVolume(false);
folders.add(folder2);
reader.next();
}
}
if (reader.getLocalName() == 'folders3') {
//stay in the loop
if (reader.hasNext()) {
reader.next();
}

while (reader.getLocalName() == 'folder'){
m_csFolder folder3 = parseFolderVolume(false);
folders.add(folder3);
reader.next();
}
}
if (reader.getLocalName() == 'folders4') {
//stay in the loop
if (reader.hasNext()) {
reader.next();
}

while (reader.getLocalName() == 'folder'){
m_csFolder folder4 = parseFolderVolume(false);
folders.add(folder4);
reader.next();
}
}
**/
                        
                        if (reader.getLocalName() == 'volumes') {
                            //stay in the loop
                            if (reader.hasNext()) {
                                reader.next();
                            }
                            
                            while (reader.getLocalName() == 'volume'){
                                m_csFolder folder5 = parseFolderVolume(true, report.name);
                                folders.add(folder5);
                                reader.next();
                            }
                        }
                    }
                }
                
                
                if (reader.hasNext()) {
                    if (reader.getEventType() == XmlTag.END_ELEMENT){
                        if (reader.getLocalName() == 'xml'){
                            isSafeToGetNextXmlElement = false;
                            break;
                        }
                    }
                    reader.next();
                } else {
                    isSafeToGetNextXmlElement = false;
                    break;
                }
                
            }
        }catch(Exception e) {
            whereAmI += 'error here ' + e.getMessage();
        }
        
        report.folders = folders;
        
        return report;
    }
    
    public m_csFolder parseFolderVolume(Boolean isVolume, String strAccount){
        m_csFolder folder = new m_csFolder();
        String strFolderName = '';
        
        try{
            if (reader.hasNext()) {
                reader.next();
            }
            
            
            
            if (reader.getLocalName() == 'path'){
                reader.next();
                folder.path = reader.getText();
                
                if (!folder.path.endsWith('/')){
                    folder.path += '/';
                }
                
                strFolderName = reader.getText();
                List<String> arrFolderParts1 = strFolderName.split('/');
                
                if (arrFolderParts1.size() > 1){
                    strFolderName = arrFolderParts1[arrFolderParts1.size() - 2] + '/' + arrFolderParts1[arrFolderParts1.size() - 1];
                }
                
                folder.Name = strFolderName;
                
                
                
                reader.next();reader.next();
            }
            
            
            if (reader.getLocalName() == 'space'){
                reader.next();
                folder.space = Decimal.valueOf(reader.getText());
                reader.next();reader.next();
            }
            
            if (reader.getLocalName() == 'spaceUOM'){
                reader.next();
                folder.spaceUOM = reader.getText();
                reader.next();reader.next();
            }
            
            
            if (reader.getLocalName() == 'used'){
                reader.next();
                folder.used = Decimal.valueOf(reader.getText());
                reader.next();reader.next();
            }
            
            
            if (reader.getLocalName() == 'usedSizeUOM'){
                reader.next();
                folder.usedSizeUOM = reader.getText();
                reader.next();reader.next();
            }
            
            if (reader.getLocalName() == 'usedPercentage'){
                reader.next();
                folder.usedPercentage = Decimal.valueOf(reader.getText());
                reader.next();reader.next();
            }
            
            if (reader.getLocalName() == 'freeSize'){
                reader.next();
                folder.freeSize = Decimal.valueOf(reader.getText());
                reader.next();reader.next();
            }
            
            if (reader.getLocalName() == 'freeSizeUOM'){
                reader.next();
                folder.freeSizeUOM = reader.getText();
                reader.next();reader.next();
            }
            
            if (reader.getLocalName() == 'spaceMB'){
                reader.next();
                folder.spaceMB = Decimal.valueOf(reader.getText());
                reader.next();reader.next();
            }
            
            if (reader.getLocalName() == 'freeMB'){
                reader.next();
                folder.freeMB = Decimal.valueOf(reader.getText());
                reader.next();reader.next();
            }
            
            if (reader.getLocalName() == 'usedMB'){
                reader.next();
                folder.usedMB = Decimal.valueOf(reader.getText());
                reader.next();reader.next();
            }
            
            
            if (isVolume){
                folder.isTopLevelFolder = false;
            }
            
            if (reader.getLocalName() == 'topLevelFolder'){
                reader.next();
                folder.isTopLevelFolder = Boolean.valueOf(reader.getText());
                reader.next();
                reader.next();
            }
            
            
            folder.isVolume = isVolume;
            
        }catch(Exception e) {
            whereAmI += 'error here ' + e.getMessage();
        }
        
        return folder;
    }
    
}